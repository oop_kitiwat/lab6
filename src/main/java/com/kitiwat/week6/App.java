package com.kitiwat.week6;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        BookBank kitiwat = new BookBank("Kitiwat", 50.0);

        kitiwat.print();

        BookBank prayuth = new BookBank("Prayuth", 100000.0);
        prayuth.print();
        prayuth.withdraw(40000.0);
        prayuth.print();

        kitiwat.deposit(40000.0);
        kitiwat.print();

        BookBank prawit = new BookBank("Prawit", 1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();

    }
}
