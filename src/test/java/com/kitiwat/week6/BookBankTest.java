package com.kitiwat.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess(){
        BookBank book = new BookBank("Kitiwat", 100);
        book.withdraw(50);
        assertEquals(50.0, book.getBalance(),0.0001);
    }

    @Test
    public void shouldWithdrawOverBalance(){
        BookBank book = new BookBank("Kitiwat", 100);
        book.withdraw(150);
        assertEquals(100.0, book.getBalance(),0.0001);
    }

    @Test
    public void shouldWithdrawNegativeNumber(){
        BookBank book = new BookBank("Kitiwat", 100);
        book.withdraw(-100);
        assertEquals(100.0, book.getBalance(),0.0001);
}
    @Test
    public void shouldDepositSucces(){
        BookBank book = new BookBank("Kitiwat", 100);
        book.deposit(50);
        assertEquals(150, book.getBalance(),0.0001);
    }
    
    @Test
    public void shouldDepositNegative(){
        BookBank book = new BookBank("Kitiwat", 100);
        book.deposit(-50);
        assertEquals(100, book.getBalance(),0.0001);
    }
}
    

