package com.kitiwat.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver(){
        Robot robot = new Robot("Robot",'R',0,Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }


    @Test
    public void shouldUpNegative(){
        Robot robot = new Robot("Robot",'R',0,Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());

}
    @Test
    public void shouldDownSuccess(){
        Robot robot = new Robot("Robot",'R',0,0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }
    @Test
    public void shouldUpSuccess(){
        Robot robot = new Robot("Robot",'R',0,1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY()); 
    }
    @Test
    public void shouldLeftNegative(){
        Robot robot = new Robot("Robot",'R',Robot.MIN_X,0);
        assertEquals(false, robot.left());
        assertEquals(Robot.MIN_X, robot.getX());
    }
    @Test
    public void shouldLeftSuck(){
        Robot robot = new Robot("Robot",'R',1,0);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
    }
    @Test
    public void shouldRightSuck(){
        Robot robot = new Robot("Robot",'R',0,1);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }
    @Test
    public void shouldRightOver(){
        Robot robot = new Robot("Robot",'R',Robot.MAX_X,0);
        assertEquals(false, robot.right());
        assertEquals(Robot.MAX_X, robot.getX());
    }
    @Test
    public void shouldCreatRobotSuccess1() {
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }
    @Test
    public void shouldCreatRobotSuccess2(){
        Robot robot = new Robot("Robot",'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldUpNSuccess1(){
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals(true, robot.up(5));
        assertEquals(6, robot.getY()); 
    }
    @Test
    public void shouldUpNSuccess2(){
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals(true, robot.up(11));
        assertEquals(0, robot.getY()); 
    }
    @Test
    public void shouldUpNFail1(){
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals(false, robot.up(12));
        assertEquals(0, robot.getY()); 
    }
    @Test
    public void shouldUpNFail2(){
        Robot robot = new Robot("Robot",'R',10,0);
        assertEquals(false, robot.up(16));
        assertEquals(0, robot.getY()); 
    }
    @Test
    public void shouldDownSuccess1(){
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals(true, robot.down(5));
        assertEquals(16, robot.getY());
    }
    @Test
    public void shouldDownSuccess2(){
        Robot robot = new Robot("Robot",'R',10,5);
        assertEquals(true, robot.down(9));
        assertEquals(14, robot.getY());
    }
    @Test
    public void shouldDownFail1(){
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals(false, robot.down(12));
        assertEquals(19, robot.getY());
    }
    @Test
    public void shouldDownFail2(){
        Robot robot = new Robot("Robot",'R',10,19);
        assertEquals(false, robot.down(2));
        assertEquals(19, robot.getY());
    }
    @Test
    public void shouldLeftSuccess1(){
        Robot robot = new Robot("Robot",'R',10,0);
        assertEquals(true, robot.left(5));
        assertEquals(5, robot.getX());
    }
    @Test
    public void shouldLeftSuccess2(){
        Robot robot = new Robot("Robot",'R',19,0);
        assertEquals(true, robot.left(10));
        assertEquals(9, robot.getX());
    }
    @Test
    public void shouldLeftFail1(){
        Robot robot = new Robot("Robot",'R',5,0);
        assertEquals(false, robot.left(6));
        assertEquals(0, robot.getX());
    }
    @Test
    public void shouldLeftFail2(){
        Robot robot = new Robot("Robot",'R',15,0);
        assertEquals(false, robot.left(16));
        assertEquals(0, robot.getX());
    }
    @Test
    public void shouldRightSuccess1(){
        Robot robot = new Robot("Robot",'R',5,1);
        assertEquals(true, robot.right(5));
        assertEquals(10, robot.getX());
    }
    @Test
    public void shouldRightSuccess2(){
        Robot robot = new Robot("Robot",'R',10,1);
        assertEquals(true, robot.right(9));
        assertEquals(19, robot.getX());
    }
    @Test
    public void shouldRightFail1(){
        Robot robot = new Robot("Robot",'R',10,1);
        assertEquals(false, robot.right(10));
        assertEquals(19, robot.getX());
    }
    @Test
    public void shouldRightFail2(){
        Robot robot = new Robot("Robot",'R',0,1);
        assertEquals(false, robot.right(20));
        assertEquals(19, robot.getX());
    }



}


 
    

  


